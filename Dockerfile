FROM httpd:2.4
EXPOSE 80

RUN sed -i s'/#LoadModule cgi_module/LoadModule cgi_module/g' /usr/local/apache2/conf/httpd.conf
RUN sed -i s'/#LoadModule cgid_module/LoadModule cgid_module/g' /usr/local/apache2/conf/httpd.conf
COPY --chown=www-data:www-data shell.sh /usr/local/apache2/cgi-bin/
RUN chmod +x /usr/local/apache2/cgi-bin/shell.sh
COPY --chown=www-data:www-data index.html /usr/local/apache2/htdocs/
