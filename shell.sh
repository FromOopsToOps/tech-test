#!/bin/bash
if [ "$REQUEST_METHOD" = "POST" ]; then
  if [ "$CONTENT_LENGTH" -gt 0 ]; then
      read -n $CONTENT_LENGTH POST_DATA <&0
  fi
fi

input1=$(echo $POST_DATA |awk -F"&" '{print $1}')
input2=$(echo $POST_DATA |awk -F"&" '{print $2}')
A=$(echo $input1 |awk -F'=' '{print $NF}')
B=$(echo $input2 |awk -F'=' '{print $NF}')
RES=$(($A * $B))

echo "Content-type: text/html"
echo ""

echo "$A * $B; resultado: $RES"
